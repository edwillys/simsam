#include "SfzParser.h"
#include "SfzCommon.h"
#include <iostream>
#include <fstream>
#include <sstream> 
#include <string>
#include <cstring>
#include <stdio.h>
#include <map>
#include <algorithm>
#include <memory>

CSfzParams CSfzParser::getParams(std::string props) 
{
    std::map<std::string, std::string> map_args;
    CSfzParams params;

    // parse properties
    for (uint32_t i = 0; i < sizeof(m_KeyWords) / sizeof(m_KeyWords[0]); i++)
    {
        // find last occurrence so that the region properties have priority over group ones
        size_t ind = props.rfind(m_KeyWords[i]);
        if (ind != std::string::npos)
        {
            // start of with the index just after the "="
            auto ind_begin = ind + m_KeyWords[i].size();
            // try to find ending space
            size_t ind_end = props.size();
            for(auto i = ind_begin; i < props.size(); i++)
            {
                if (isspace(props[i]) )
                {
                    ind_end = i;
                    break;
                }
            }           
            // insert (key, val) to map, without the '=' symbol
            std::string key = m_KeyWords[i].substr(0, m_KeyWords[i].size() - 1);
            // insert to map
            map_args[key] = props.substr(ind_begin, ind_end - ind_begin);
        }
    }

    auto pair_key = map_args.find("key");
    auto pair_lokey = map_args.find("lokey");
    auto pair_hikey = map_args.find("hikey");
    auto pair_centerkey = map_args.find("pitch_keycenter");
    auto pair_ampveltrack = map_args.find("amp_veltrack");
    auto pair_release = map_args.find("ampeg_release");
    auto pair_attack = map_args.find("ampeg_attack");
    auto pair_sample = map_args.find("sample");
    auto pair_lovel = map_args.find("lovel");
    auto pair_hivel = map_args.find("hivel");

    // Extract direct integer values, if given
    if ( pair_ampveltrack != map_args.end() )
        params.ampveltrack = stoi(pair_ampveltrack->second);

    if ( (pair_lovel == map_args.end()) && (pair_hivel == map_args.end()) )
    {
        // none available
        params.lovel = 0;
        params.hivel = 127;
    }
    else if ( pair_lovel == map_args.end() )
    {
        // only hivel available
        params.hivel = stoi(pair_hivel->second);
        params.lovel = params.hivel;
    }
    else if ( pair_hivel == map_args.end() )
    {
        // only lovel available
        params.lovel = stoi(pair_lovel->second);
        params.hivel = params.lovel;
    }
    else
    {
        // both available
        params.hivel = stoi(pair_hivel->second);
        params.lovel = stoi(pair_lovel->second);
    }
    
    
    // Extract direct float values, if given
    //if ( pair_attack != map_args.end() )
    //    params.attack_s = stof(pair_attack->second);
    //if ( pair_release != map_args.end() )
    //    params.release_s = stof(pair_release->second);

    // Extract direct string values, if given
    if ( pair_sample != map_args.end() )
    {
        // compose with base dir
        params.path = m_BasePath + pair_sample->second;
        // trim path
        rtrim(params.path);
        // replace backslashes with forward ones
        replace(params.path.begin(), params.path.end(), '\\', '/');
    }

    // Key is a bit trickier, as we need to extract low and high values, if they are available
    if ( (pair_lokey != map_args.end()) && (pair_hikey != map_args.end()) )
    {
        params.lokey = stoi(pair_lokey->second);
        params.hikey = stoi(pair_hikey->second);
    }
    else if ( (pair_lokey != map_args.end()) && (pair_hikey == map_args.end()) )
    {
        params.lokey = stoi(pair_lokey->second);
        params.hikey = params.lokey;
    }
    else if ( (pair_lokey == map_args.end()) && (pair_hikey != map_args.end()) )
    {
        params.hikey = stoi(pair_hikey->second);
        params.lokey = params.hikey;
    } 
    else if ( pair_centerkey != map_args.end() )
    {
        params.hikey = stoi(pair_centerkey->second);
        params.lokey = params.hikey;
    }
    else if ( pair_key != map_args.end() )
    {
        params.lokey = stoi(pair_key->second);
        params.hikey = params.lokey;
    }

    return params;
}

CSfzParser::eParseState CSfzParser::doGroup(std::string const& line) 
{
    eParseState next_state = ST_GROUP;

    // assume max of one <group> 
    size_t ind_grp = line.find(m_ArgGroup);
    size_t ind_reg = line.find(m_ArgRegion);

    if (ind_grp != std::string::npos)
    {
        // found group, simply init with content
        m_PropsGroup = line.substr(ind_grp + m_ArgGroup.size());
    }
    else if (ind_reg != std::string::npos)
    {
        // found region
        next_state = doRegion(line);
    }
    else
    {
        // add current line to the incomplete group
        m_PropsGroup += line;
    }

    return next_state;
}

CSfzParser::eParseState CSfzParser::doRegion(std::string const& line) 
{
    eParseState next_state = ST_REGION;
    // assume max of one <region> 
    size_t ind_grp = line.find(m_ArgGroup);
    size_t ind_reg = line.find(m_ArgRegion);

    if (ind_grp != std::string::npos)
    {
        // found group
        next_state = doGroup(line);
    }
    else if (ind_reg != std::string::npos)
    {
        // found new region
        if (!m_PropsRegion.empty())
        {
            // if something was there before, parse note
            doNote(m_PropsGroup + m_PropsRegion);
        }
        // init with newly found region
        m_PropsRegion = line.substr(ind_reg + m_ArgRegion.size());
    }
    else
    {
        // add current line to the incomplete region
        m_PropsRegion += line;
    }

    return next_state;
}

void CSfzParser::doNote(std::string const& props)
{
    auto params = getParams(props);
    if ( (params.lokey > 0) && ( params.hikey > 0) )
    {
        int32_t pitch = (params.lokey + params.hikey) / 2;
        std::shared_ptr<CSample> sample(new CSample(params));
        m_Notes[pitch].push_back(sample);
    }
} 

CSfzParser::eParseResult CSfzParser::parse(std::string path) 
{
    eParseResult retval = RES_OK;
    
    std::cout << "Start parsing file " << path << std::endl;
    std::ifstream sfz_file(path);

    if( !sfz_file.is_open() )
    {
        std::cerr << "Failed to open SFZ file." << std::endl;
        retval = RES_LOAD_FAIL;
    }
    else
    {
        try
        {
            std::string line;
            // out for a clear start...
            m_PropsRegion = "";
            m_PropsGroup = "";
            m_Notes.clear();
            // find folder from path
            auto ind = path.find_last_of('\\');
            if (ind == std::string::npos)
                ind = path.find_last_of('/');
            if (ind != std::string::npos)
                m_BasePath = path.substr(0, ind + 1);
            // go through SFZ file
            bool_t block_comment = false;
            while(sfz_file.peek() != EOF)
            {
                // if nothing else to be processed in this line, get a new one
                if (line.empty())
                    std::getline(sfz_file, line);
                // if we're inside of a block comment, check if we've found an end to it
                if (block_comment)
                {
                    auto ind_cmt = line.find("*/");
                    if (ind_cmt < line.size())
                    {
                        block_comment = false; // no longer in a block comment
                        // take line after that. If there is nothing, line will be empty
                        line = line.substr(ind_cmt + 2);
                    }
                    else
                    {
                        line = ""; // get next line
                    }
                }
                else
                {
                    std::size_t ind_cmt;
                    // check for beginning of block comment
                    ind_cmt = line.find("/*");
                    if (ind_cmt < line.size())
                    {
                        line = line.substr(ind_cmt + 2);
                        block_comment = true;
                    }
                    else
                    {
                        // ignore line comments: everything after '//'. here we don't take into consideration valid 
                        // occurrences of '//', for example within not well defined paths
                        ind_cmt = line.find("//");
                        line = line.substr(0, ind_cmt);
                        
                        if(!line.empty())
                        {
                            switch(m_State)
                            {
                                case ST_GROUP:
                                {
                                    m_State = doGroup(line);
                                }
                                break;

                                case ST_REGION:
                                {
                                    m_State = doRegion(line);
                                }
                                break;

                                case ST_IDLE:
                                default:
                                {
                                    if (line.find(m_ArgGroup) != std::string::npos)
                                        m_State = doGroup(line);
                                    else if (line.find(m_ArgRegion) != std::string::npos)
                                        m_State = doRegion(line);
                                }
                                break;
                            }
                            // Here we assume the line was completely processed and delete it
                            line = "";
                        }
                    }
                }
            }
            // finish up unfinished business
            doNote(m_PropsGroup + m_PropsRegion);
            // sort all entries according to lower pitch
            auto comp = [](std::shared_ptr<CSample> a, std::shared_ptr<CSample> b) { return *a < *b; };
            for(auto it = m_Notes.begin(); it != m_Notes.end(); it++)
            {
                std::sort(it->second.begin(), it->second.end(), comp);
            }
        }
        catch(const std::exception& e)
        {
            std::cerr << e.what() << '\n';
            retval = RES_OTHER_FAIL;
        }
    }
    sfz_file.close();
    
    return retval;
}

int32_t CSfzParser::getNumberOfSamples(void) 
{
    int32_t sum = 0;

    for (auto it = m_Notes.begin(); it != m_Notes.end(); ++it )
    {
        auto val = it->second;
        sum += (int32_t)val.size();
    }

    return sum;
}

