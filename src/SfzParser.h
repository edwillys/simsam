#pragma once

#include <string>
#include <vector>
#include <array>
#include <stdint.h>
#include <map>
#include "Sample.h"
#include <algorithm>
#include "SfzCommon.h"
#include <memory>

class CSfzParser
{
private:
    typedef enum
    {
        ST_IDLE = 0,
        ST_GROUP,
        ST_REGION
    }eParseState;

    std::map<int32_t, std::vector< std::shared_ptr<CSample>> > m_Notes;
    std::vector<int16_t> m_Dummy;
    eParseState m_State;

    std::string m_BasePath;
    std::string m_PropsRegion;
    std::string m_PropsGroup;
    // 
    const std::string m_ArgGroup = "<group>";
    const std::string m_ArgRegion = "<region>";

    const std::string m_KeyWords[10] = {
        "amp_veltrack=",
        "ampeg_attack=",
        "ampeg_release=",
        "sample=",
        "lokey=",
        "hikey=",
        "key=",
        "pitch_keycenter=",
        "lovel=",
        "hivel="
    };

    /**
     * @brief Get the Params object
     * 
     * @param props 
     * @return CSfzParams 
     */
    CSfzParams getParams(std::string props);

    /**
     * @brief Trim from start (in place)
     * 
     * @param s string to trim
     */
    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
            return !std::isspace(ch);
        }));
    }

    /**
     * @brief Trim from end (in place)
     * 
     * @param s string to trim
     */
    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
            return !std::isspace(ch);
        }).base(), s.end());
    }
public:
    typedef enum
    {
        RES_OK = 0,
        RES_LOAD_FAIL,
        RES_OTHER_FAIL
    }eParseResult;

    /**
     * @brief Construct a new CSfzParser object
     * 
     */
    CSfzParser() : m_State(ST_IDLE) {};

    /**
     * @brief Destroy the CSfzParser object
     * 
     */
    ~CSfzParser() {};
    
    /**
     * @brief 
     * 
     * @param path 
     * @return eParseResult 
     */
    eParseResult parse(std::string path);
    
    /**
     * @brief 
     * 
     * @param line 
     * @return eParseState 
     */
    eParseState doGroup(std::string const& line);
    
    /**
     * @brief 
     * 
     * @param line 
     * @return eParseState 
     */
    eParseState doRegion(std::string const& line);
    
    /**
     * @brief 
     * 
     * @param props 
     */
    void doNote(std::string const& props);
    
    /**
     * @brief Get the Notes object
     * 
     * @return const std::map<int32_t, std::vector<std::shared_ptr<CSample>> >& 
     */
    const std::map<int32_t, std::vector<std::shared_ptr<CSample>> >& getNotes(void){ return m_Notes; };
    
    /**
     * @brief Get the Number Of Samples object
     * 
     * @return int32_t 
     */
    int32_t getNumberOfSamples(void);
};
