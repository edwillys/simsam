/**
 * @file SfzCommon.h
 * @author Edgar Lubicz (edgarlubicz@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2020-04-27
 * 
 * @copyright Copyright (c) 2020
 * 
 */

#pragma once
#include <string>
#include "AudioTypes.h"

/**
 * @brief 
 * 
 */
class CSfzParams
{
public:
    std::string path;
    int32_t   lokey;
    int32_t   hikey;
    int32_t   lovel;
    int32_t   hivel;
    int32_t   ampveltrack;
    float32_t attack_s;
    float32_t decay_s;
    float32_t release_s;
    float32_t decay_gain;
    int32_t   fs;
    bool_t    append;

    /**
     * @brief Construct a new CSfzParams object
     * 
     */
    CSfzParams() : 
        lokey(0), hikey(0), lovel(0), hivel(0), decay_gain(1.0F),
        ampveltrack(0), attack_s(0.F), decay_s(0.0F), release_s(0.F), fs(48000),
        append(true)
        {};
    
    /**
     * @brief Destroy the CSfzParams object
     * 
     */
    ~CSfzParams() {};
};
