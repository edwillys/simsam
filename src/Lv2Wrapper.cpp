/*************************************************************************************************/
/*                                          Includes                                             */
/*************************************************************************************************/
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include "lv2/lv2plug.in/ns/lv2core/lv2.h"
#include "lv2/lv2plug.in/ns/ext/midi/midi.h"
#include "lv2/lv2plug.in/ns/ext/urid/urid.h"
#include "lv2/lv2plug.in/ns/ext/atom/atom.h"
#include "lv2/lv2plug.in/ns/ext/atom/util.h"
#include "lv2/lv2plug.in/ns/ext/log/log.h"
#include "lv2/lv2plug.in/ns/ext/log/logger.h"
#include "lv2/lv2plug.in/ns/ext/worker/worker.h"
#include "SimSam.h"
#include "AudioTypes.h"

/*************************************************************************************************/
/*                                          Defines                                              */
/*************************************************************************************************/
#define SFZP_URI "https://gitlab.com/edwillys/simsam/"

#define FLOAT_ISEQUAL(a,b) fabs(a - b) > 0.0F 

typedef enum
{
    SFZP_ATTACK,
    SFZP_DECAY,
    SFZP_RELEASE,
    SFZP_SUSTAIN_LEVEL,
    SFZP_FADEOUT,
    SFZP_POLYPHONY,
    SFZP_SFZPATH_ID,
    SFZP_VEL_INTERPOL,
    SFZP_MIDI,
    SFZP_OUTPUT,
    SFZP_STATUS
} eParamIndex;

typedef enum
{
    STATUS_READY = 0,
    STATUS_LOADING_SAMPLES,
    STATUS_LOADING_FAILED
} eSfzStatus;

typedef struct 
{
    LV2_URID midi_event;
    LV2_URID new_sfz_event;
} tURIs;

/**
 * @brief An atom-like message used internally
 * 
 */
typedef struct {
    LV2_Atom atom;
    CSimSam* sfzp;
} tMessage;

typedef struct
{
    // Ports
    float32_t *output;
    const LV2_Atom_Sequence* control_port;
        
    // Parameters
    float32_t *attack;
    float32_t *decay;
    float32_t *release;
    float32_t *sustain_level;
    float32_t *fadeout;
    float32_t *polyphony_ind;
    float32_t *sfzpath_ind;
    float32_t *velocity_interpol;
    float32_t *sfz_status;
    
    // Previous states of parameters
    CSample::CADSR ctrl_adsr;
    float32_t ctrl_fadeout;
    uint32_t ctrl_polyphony_ind;
    uint32_t ctrl_sfzpath_ind;
    uint32_t ctrl_velocity_interpol;

    // URIS
    tURIs uris;

    // Features
    LV2_URID_Map *map;
    LV2_Worker_Schedule *schedule;
    LV2_Log_Log *log;
    
    // Logger convenience API
    LV2_Log_Logger logger;
    
    std::vector <int32_t> midi_payload;
    std::string bundle_path;

    // Main object
    CSimSam *sfzp;
} tLV2SimSamHnd;

// Function prototypes

/**
 * @brief 
 * 
 * @param descriptor 
 * @param rate 
 * @param bundle_path 
 * @param features 
 * @return LV2_Handle 
 */
static LV2_Handle instantiate(const LV2_Descriptor *descriptor, double rate,
                              const char *bundle_path, const LV2_Feature *const *features);

/**
 * @brief 
 * 
 * @param instance 
 * @param port 
 * @param data 
 */
static void connect_port(LV2_Handle hnd, uint32_t port, void *data);

/**
 * @brief 
 * 
 * @param instance 
 */
static void activate(LV2_Handle hnd);

/**
 * @brief 
 * 
 * @param instance 
 */
static void deactivate(LV2_Handle hnd);

/**
 * @brief 
 * 
 * @param instance 
 */
static void cleanup(LV2_Handle hnd);

/**
 * @brief 
 * 
 * @param uri 
 * @return const void* 
 */
static const void *extension_data(const char *uri);

/**
 * @brief 
 * 
 * @param instance 
 * @param n_samples 
 */
static void run(LV2_Handle hnd, uint32_t n_samples);

/**
 * @brief 
 * 
 * @param hnd 
 * @param respond 
 * @param handle 
 * @param size 
 * @param data 
 * @return LV2_Worker_Status 
 */
static LV2_Worker_Status
work(LV2_Handle                  hnd,
     LV2_Worker_Respond_Function respond,
     LV2_Worker_Respond_Handle   handle,
     uint32_t                    size,
     const void*                 data);

/**
 * @brief 
 * 
 * @param instance 
 * @param size 
 * @param data 
 * @return LV2_Worker_Status 
 */
static LV2_Worker_Status
work_response(LV2_Handle  instance,
              uint32_t    size,
              const void* data);

/*************************************************************************************************/
/*                                          Globals                                              */
/*************************************************************************************************/
static const LV2_Descriptor SfzpDescriptor = {
    SFZP_URI,
    instantiate,
    connect_port,
    activate,
    run,
    deactivate,
    cleanup,
    extension_data
};

static const std::string SfzPaths[] = {
    "/sfz/salamandergrandpiano/salamander.sfz",
    "/sfz/francisbaconpiano/francis_bacon.sfz",
    "/sfz/SteinwaySonsModelB/steinway_sons.sfz"
};

static const int32_t PolyphonyVals[] = {
    8,
    32
};

/*************************************************************************************************/
/*                                          Functions                                            */
/*************************************************************************************************/
static LV2_Handle
instantiate(const LV2_Descriptor *descriptor,
            double rate,
            const char *bundle_path,
            const LV2_Feature *const *features)
{
    tLV2SimSamHnd *self = new tLV2SimSamHnd(); // init struct with zeros

    if (NULL != self)
    {
        self->sfzp = new CSimSam();
        self->midi_payload.resize(3);
        // init control values to invalid values so that they get updated
        self->ctrl_adsr.att_s = -1.F;
        self->ctrl_fadeout = -1.0F;
        self->ctrl_polyphony_ind = UINT32_MAX;
        self->ctrl_sfzpath_ind = UINT32_MAX;
        self->ctrl_velocity_interpol = UINT32_MAX;

        // features
        for (int i = 0; features[i]; ++i)
        {
            if (!strcmp(features[i]->URI, LV2_URID__map))
                self->map = (LV2_URID_Map *)features[i]->data;
            else if (!strcmp(features[i]->URI, LV2_WORKER__schedule))
                self->schedule = (LV2_Worker_Schedule *)features[i]->data;
            else if (!strcmp(features[i]->URI, LV2_LOG__log))
                self->log = (LV2_Log_Log *)features[i]->data;
        }
        
        if (NULL != self->map && NULL != self->schedule)
        {
            // Map URIs and initialise forge/logger
            self->uris.midi_event = self->map->map(self->map->handle, LV2_MIDI__MidiEvent);
            self->uris.new_sfz_event = self->map->map(self->map->handle, "NewSFZ");
            lv2_log_logger_init(&self->logger, self->map, self->log);
            
            // add bundle path
            self->bundle_path = std::string(bundle_path);

            CSimSamProps props;
            props.m_BlockSize = 512;
            props.m_Fs = (int32_t)rate;
            props.m_NumChOut = 1;
            props.m_NumCtrlIn = 1;
            props.m_FilePath = "";
            
            self->sfzp->init(props); 
            // samples will be loaded on the worker thread
        }
        else
        {
            if (NULL == self->map)
                lv2_log_error(&self->logger, "Missing 'map' feature. \n");
            if (NULL == self->schedule)
                lv2_log_error(&self->logger, "Missing 'schedule' feature. \n");
            lv2_log_error(&self->logger, "Quitting... \n");
            delete self;
            self = NULL;
        }
    }

    return (LV2_Handle)self;
}

static void
connect_port(LV2_Handle hnd,
             uint32_t port,
             void *data)
{
    tLV2SimSamHnd *self = (tLV2SimSamHnd *)hnd;

    switch ((eParamIndex)port)
    {
        case SFZP_ATTACK:
        self->attack = (float32_t *)data;
        break;

        case SFZP_DECAY:
        self->decay = (float32_t *)data;
        break;

        case SFZP_RELEASE:
        self->release = (float32_t *)data;
        break;
        
        case SFZP_SUSTAIN_LEVEL:
        self->sustain_level = (float32_t *)data;
        break;
        
        case SFZP_FADEOUT:
        self->fadeout = (float32_t *)data;
        break;

        case SFZP_POLYPHONY:
        self->polyphony_ind = (float32_t *)data;
        break;

        case SFZP_SFZPATH_ID:
        self->sfzpath_ind = (float32_t *)data;
        break;

        case SFZP_VEL_INTERPOL:
        self->velocity_interpol = (float32_t *)data;
        break;

        case SFZP_MIDI:
        self->control_port = (const LV2_Atom_Sequence *)data;
        break;

        case SFZP_OUTPUT:
        self->output = (float32_t *)data;
        break;

        case SFZP_STATUS:
        self->sfz_status = (float32_t *)data;;
        break;

        default:
        break;
    }
}

static LV2_Worker_Status
work(LV2_Handle                  hnd,
     LV2_Worker_Respond_Function respond,
     LV2_Worker_Respond_Handle   handle,
     uint32_t                    size,
     const void*                 data)
{
    tLV2SimSamHnd*  self = (tLV2SimSamHnd*)hnd;
    const LV2_Atom* atom = (const LV2_Atom*)data;
    if (atom->type == self->uris.new_sfz_event) 
    {
        // Load new SFZ file
        const tMessage* msg = (const tMessage*)data;
        auto res = msg->sfzp->loadSamples();
        if ( 0 != res)
        {
            // Inform UI that we failed to load the samples
            *self->sfz_status = STATUS_LOADING_FAILED;
            lv2_log_error(&self->logger, "Failed to load samples: %d\n", res);
        }
        else
        {
            // invalidate control parameters so that they are loaded on the next real time cycle
            self->ctrl_adsr.att_s = -1.F;
            self->ctrl_fadeout = -1.0F;
            self->ctrl_polyphony_ind = UINT32_MAX;
            self->ctrl_velocity_interpol = UINT32_MAX;
            // Inform UI that we finished loading the samples
            *self->sfz_status = STATUS_READY;
        }
    }

    return LV2_WORKER_SUCCESS;
}

static LV2_Worker_Status
work_response(LV2_Handle  instance,
              uint32_t    size,
              const void* data)
{
    return LV2_WORKER_SUCCESS;
}

static void
activate(LV2_Handle hnd)
{
}

static void
deactivate(LV2_Handle hnd)
{
}

static void
cleanup(LV2_Handle hnd)
{
    tLV2SimSamHnd *sfzph = (tLV2SimSamHnd *)hnd;
    delete sfzph->sfzp;
    delete sfzph;
}

static const void *
extension_data(const char *uri)
{
    static const LV2_Worker_Interface worker = { work, work_response, NULL };
    
    if (!strcmp(uri, LV2_WORKER__interface))
        return &worker;

    return NULL;
}

static void
run(LV2_Handle hnd, uint32_t n_samples)
{
    tLV2SimSamHnd * const self = (tLV2SimSamHnd * const)hnd;
    auto sfzp = self->sfzp;
    tURIs* uris = &self->uris;
    
    if(!self->sfzp->isLoadPending())
    {
        // update the blocksize, if we have to...
        const CAudioAtomProps& props = sfzp->getProps();
        if( props.m_BlockSize != n_samples)
        {
            //std::cout << "New blocksize: n_samples=" << n_samples << std::endl;
            auto new_props = props;
            new_props.m_BlockSize = n_samples;
            sfzp->setProps(new_props);
        }

        // Update SFZ path, if needed
        uint32_t sfz_ind = (uint32_t)*self->sfzpath_ind;
        if (self->ctrl_sfzpath_ind != *self->sfzpath_ind 
            && sfz_ind < sizeof(SfzPaths) / sizeof(SfzPaths[0]) )
        {
            //std::cout << "New sfz path=" << SfzPaths[sfz_ind] << std::endl;
            
            // Inform UI that we are about to load the samples
            *self->sfz_status = STATUS_LOADING_SAMPLES;

            // TODO: extract the SFZ path from the ID
            self->sfzp->setFilePath(self->bundle_path + SfzPaths[sfz_ind]);

            tMessage msg = { { sizeof(CSimSam*), self->uris.new_sfz_event },
                                self->sfzp };
            // Send a message to the worker to load the samples from the new SFZ file
            self->schedule->schedule_work(self->schedule->handle, sizeof(msg), &msg);
            self->ctrl_sfzpath_ind = sfz_ind;

            return; // return immediately
        }

        // Update polyphony_ind if needed
        uint32_t pol_ind = (uint32_t)*self->polyphony_ind;
        if (self->ctrl_polyphony_ind != pol_ind 
            && pol_ind < sizeof(PolyphonyVals) / sizeof(PolyphonyVals[0]) )
        {
            //std::cout << "New polyphony=" << PolyphonyVals[pol_ind] << std::endl;
            self->sfzp->setPolyphony(PolyphonyVals[pol_ind]);
            self->ctrl_polyphony_ind = pol_ind;
        }

        // update the ADSR, if we have to...
        auto att_s   = 0.001F * *self->attack;
        auto dec_s   = 0.001F * *self->decay;
        auto sus_lvl = *self->sustain_level;
        auto rel_s   = 0.001F * *self->release;
        if( FLOAT_ISEQUAL(self->ctrl_adsr.att_s  , att_s  ) 
        || FLOAT_ISEQUAL(self->ctrl_adsr.dec_s  , dec_s  ) 
        || FLOAT_ISEQUAL(self->ctrl_adsr.sus_lvl, sus_lvl) 
        || FLOAT_ISEQUAL(self->ctrl_adsr.rel_s  , rel_s  ) )
        {
            //std::cout << "New ADSR. att=" << att_s << " dec=" << dec_s << " sus=" << sus_lvl << " rel=" << rel_s << std::endl;
            self->ctrl_adsr = {att_s, dec_s, sus_lvl, rel_s};
            sfzp->setADSR( self->ctrl_adsr );
        }

        // update fadeout if needed 
        auto fadeout_s = 0.001F * *self->fadeout;
        if( FLOAT_ISEQUAL(self->ctrl_fadeout, fadeout_s ) )
        {
            //std::cout << "New fadeout=" << fadeout_s << std::endl;
            self->ctrl_fadeout = fadeout_s;
            sfzp->setFadeout( self->ctrl_fadeout );
        }

        // update velocity modulation if needed
        if( self->ctrl_velocity_interpol != (uint32_t)*self->velocity_interpol )
        {
            //std::cout << "New velocity interpol=" << (uint32_t)*self->velocity_interpol << std::endl;
            self->ctrl_velocity_interpol = (uint32_t)*self->velocity_interpol;
            // TODO
        }

        // Read and process incoming events
        LV2_ATOM_SEQUENCE_FOREACH(self->control_port, ev)
        {
            if (ev->body.type == uris->midi_event)
            {
                const uint8_t *const msg = (const uint8_t *)(ev + 1);
                auto type = lv2_midi_message_type(msg); 
                switch (type)
                {
                    case LV2_MIDI_MSG_NOTE_ON:
                    {
                        self->midi_payload[0] = msg[1];
                        self->midi_payload[1] = CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON;
                        self->midi_payload[2] = msg[2];
                        sfzp->set(CSimSam::ESetIntVectorIndex::SET_INTVEC_MIDI, self->midi_payload);
                    }
                    break;
        
                    case LV2_MIDI_MSG_NOTE_OFF:
                    {
                        self->midi_payload[0] = msg[1];
                        self->midi_payload[1] = CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_OFF;
                        sfzp->set(CSimSam::ESetIntVectorIndex::SET_INTVEC_MIDI, self->midi_payload);
                    }
                    break;
                    
                    default:
                    break;
                }
            }
            else
            {
                lv2_log_trace(&self->logger, "Unknown event type %d\n", ev->body.type);
            }
        }
        float32_t *outputs[1];
        outputs[0] = self->output;
        sfzp->play(NULL, outputs);

    }

}

LV2_SYMBOL_EXPORT
const LV2_Descriptor *
lv2_descriptor(uint32_t index)
{
    switch (index)
    {
        case 0:
        return &SfzpDescriptor;
        default:
        return NULL;
    }
}
