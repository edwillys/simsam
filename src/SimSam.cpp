#include "SimSam.h"
#include "SfzParser.h"
#include <vector>
#include <algorithm>
#include <chrono>
#include <iostream>

namespace chr = std::chrono;

void CSimSam::init(const CSimSamProps& props) 
{
    CAudioAtom::init(props);
    
    m_LoadPending = false;
    m_Polyphony = props.m_Polyphony;
    
    if ( !props.m_FilePath.empty() )
        setFilePath(props.m_FilePath);
}

int32_t CSimSam::loadSamples(void)
{
    // let us be optimistic
    int32_t retval = CSfzParser::RES_OK;
    CSfzParser sfzp = CSfzParser();

    std::cout << "Started loading samples " << m_Path << std::endl;
    auto start_time = chr::duration_cast< chr::milliseconds >
        (chr::system_clock::now().time_since_epoch()).count();
    
    retval = sfzp.parse(m_Path);
    
    auto end_time = chr::duration_cast< chr::milliseconds >
        (chr::system_clock::now().time_since_epoch()).count();
    // get notes and copy them to local struct
    if( retval == CSfzParser::RES_OK)
    {
        auto notes = sfzp.getNotes();
        m_Notes = notes;
        auto len = notes.size();
        std::cout << "Finished loading " << len << " notes in " << end_time - start_time << "ms" << std::endl;
        if (len > 0)
        {
            auto lower_pitch = m_Notes.begin()->first;
            auto higher_pitch = m_Notes.rbegin()->first;
            std::vector<int32_t>resampled;
            for( auto i = lower_pitch + 1; i < higher_pitch; i++)
            {
                if (!getRefPitch(i))
                {
                    // resampling map variable, ascending order by size of polyphase to optimize CPU
                    struct
                    {
                        int32_t ind;
                        int32_t us;
                        int32_t ds;
                        std::string desc;
                    } resampling_map[] = {
                        {i + 7, 3, 2, "fifth above" } , // size of polyphase = ceil(61/3)=21
                        {i + 5, 4, 3, "fourth above"} , // size of polyphase = ceil(81/4)=21
                        {i - 5, 3, 4, "fourth below"} , // size of polyphase = ceil(61/3)=27
                        {i - 7, 2, 3, "fifth below" }   // size of polyphase = ceil(61/2)=31
                    };

                    auto j = 0;
                    for (; j < sizeof(resampling_map) / sizeof(resampling_map[0]); j++)
                    {
                        auto el = resampling_map[j];
                        auto ind = el.ind;
                        auto ref_pitch = getRefPitch(ind);
                        if (std::find(resampled.begin(), resampled.end(), ind) == resampled.end() 
                            && ref_pitch > 0)
                        {
                            //std::cout << "Pitch " << i << " is not a note. Resampling " << el.desc << std::endl;
                            std::vector<std::shared_ptr<CSample> > samples;
                            for(auto it = m_Notes[ref_pitch].begin(); it != m_Notes[ref_pitch].end(); it++)
                            {
                                CSample& ref = **it;
                                std::shared_ptr<CSample> smp(new CSample(ref, i, i, el.us, el.ds));
                                samples.push_back(smp);
                            }
                            m_Notes[i] = samples;
                            resampled.push_back(i);
                            break;
                        }
                    }
                    if( j == sizeof(resampling_map) / sizeof(resampling_map[0]))
                    {
                        std::cout << "Pitch " << i << " is not a note. No upper/lower 5th/4th found." << std::endl;
                    }
                }
            }
        }
        m_LoadPending = false;
    }
    else
    {
        std::cout << "Error in loading samples" << std::endl;
    }

    return retval;
}

void CSimSam::setFilePath(const std::string &path)
{
    m_LoadPending = true;
    m_Path = path;
}

void CSimSam::play(float32_t ** const in, float32_t ** const out) 
{
    if (NULL != out)
    {
        // zero out output first of all
        for(auto ch = 0; ch < m_Props.m_NumChOut; ch++)
            for(auto i = 0; i < m_Props.m_BlockSize; i++)
                out[ch][i] = 0.0F;
        
        if (!m_LoadPending)
        {
            std::vector<std::shared_ptr<CSample> > still_playing;
            for(auto ch = 0; ch < m_Props.m_NumChOut; ch++)
            {
                float32_t *in = out[ch];
                for(auto i = 0; i <m_PlayingNotes.size(); i++)
                {
                    m_PlayingNotes[i]->play(in, m_Props.m_BlockSize);
                    // check if sample finished playing
                    if( m_PlayingNotes[i]->getNumSamplesUntilFinished() > 0)
                        still_playing.push_back(m_PlayingNotes[i]);
                }
            }
            // replace the vector with notes that are still playing
            m_PlayingNotes = still_playing;
        }
    }
}

void CSimSam::noteOn(std::shared_ptr<CSample> sample) 
{
    if( NULL != sample)
    {
        bool_t note_playing = false;
        
        // check if note isn't already playing, if so, restart it
        for(auto it = m_PlayingNotes.begin(); it != m_PlayingNotes.end(); it++)
        {
            if ( *(*it) == *sample )
            {
                // replace pointer, as it could have a different velocity
                (*it) = sample;
                // turn it on
                (*it)->on();
                note_playing = true;
                break;
            }
        }
        // if new note, proceed
        if (!note_playing)
        {
            // remove note that is closest to end, in case we reached polyphony limit
            if (m_PlayingNotes.size() == m_Polyphony)
            {
                auto it_min = m_PlayingNotes.end();
                int32_t val_min = INT32_MAX;
                for(auto it = m_PlayingNotes.begin(); it != m_PlayingNotes.end(); it++)
                {
                    auto missing_samples = (*it)->getNumSamplesUntilFinished();
                    if (missing_samples < val_min)
                    {
                        it_min = it;
                        val_min = missing_samples;
                    }
                }
                if (it_min != m_PlayingNotes.end())
                    m_PlayingNotes.erase(it_min);
            }

            // turn on the note
            sample->on();
            // add to the vector
            m_PlayingNotes.push_back(sample);   
        } 
    }
}

void CSimSam::noteOff(cint32_t pitch) 
{
    // search for the note to release and release it
    for(auto it = m_PlayingNotes.begin(); it != m_PlayingNotes.end(); it++)
    {
        auto lokey = (*it)->getLowKey();
        auto hikey = (*it)->getHighKey();
        if ( lokey <= pitch && hikey >= pitch )
        {
            (*it)->off();
            // TODO: break here?
        }
    }
}


cint32_t CSimSam::getRefPitch(cint32_t pitch) 
{
    for(auto it = m_Notes.begin(); it != m_Notes.end(); it++)
    {
        auto vsample = it->second;
        for( auto it2 = vsample.begin(); it2 != vsample.end(); it2++)
        {
            auto lokey = (*it2)->getLowKey();
            auto hikey = (*it2)->getHighKey();
            if (pitch >= lokey && pitch <= hikey)
            {
                return it->first;
            }
            else if (hikey > pitch )
            {
                // since the map is ordered by pitch, if we are passed it, it means it doesn't belong here
                return 0;
            }
        }
    }

    return 0;
}

void CSimSam::midiOn(cint32_t pitch, cint32_t velocity) 
{
    // search for note
    std::vector<std::shared_ptr<CSample> > *vsample = NULL;
    for(auto it = m_Notes.begin(); it != m_Notes.end(); it++)
    {
        auto lokey = it->second[0]->getLowKey();
        auto hikey = it->second[0]->getHighKey();
        if ( (pitch >= lokey) && (pitch <= hikey))
        {
            vsample = &it->second;
            break;
        }
    }

    // search for velocity
    if (NULL != vsample && vsample->size() > 0)
    {
        bool_t note_found = false;
        auto first = *(*vsample).begin();
        auto last = *(*vsample).rbegin();

        for(auto it = (*vsample).begin(); it != (*vsample).end(); it++)
        {
            auto lovel = (*it)->getLowVel();
            auto hivel = (*it)->getHighVel();
            if ( (velocity >= lovel) && (velocity <= hivel))
            {
                noteOn(*it);
                note_found = true;
                break;
            }
        }
        if (!note_found)
            std::cout <<"Velocity " << velocity << " not found for pitch " << pitch << std::endl;
    }
    else
    {
        std::cout <<"Pitch " << pitch << " not found within samples" << std::endl;
    }
}

void CSimSam::midiOff(cint32_t pitch) 
{
    noteOff(pitch);
}

void CSimSam::set(cint32_t index, const std::vector <float32_t>& values) 
{
    switch (index)
    {
        case SET_FLOATVEC_ADSP:
        {
            if (values.size() == 4)
            {
                setADSR({values[0], values[1], values[2], values[3]});
            }
        }
        break;
    
        case SET_FLOATVEC_FADEOUT:
        {
            if (values.size() == 1)
            {
                setFadeout(values[0]);
            }
        }
        break;

        default:
        break;
    }
}

void CSimSam::set(cint32_t index, const std::vector <int32_t>& values) 
{
    switch (index)
    {
        case SET_INTVEC_MIDI:
        {
            int32_t i = 0;
            cint32_t size = values.size();
            while ( i < ( size - 1 ) )
            {
                auto pitch = values[ i++ ];
                auto type  = values[ i++ ];
                if (type == SET_MIDI_NOTE_ON && i < size )
                {
                    auto velocity = values[ i++ ];
                    midiOn(pitch, velocity);
                }
                else if (type == SET_MIDI_NOTE_OFF)
                    midiOff(pitch);

            }
        }
        break;
    
        default:
        break;
    }
}

void CSimSam::setADSR(const CSample::CADSR& adsr)
{
    // set ADSR for all notes
    for(auto itn = m_Notes.begin(); itn != m_Notes.end(); itn++)
        for(auto its = itn->second.begin(); its != itn->second.end(); its++)
            (*its)->setADSR(adsr);
}

void CSimSam::setFadeout(cfloat32_t fade_s)
{
    // set ADSR for all notes
    for(auto itn = m_Notes.begin(); itn != m_Notes.end(); itn++)
        for(auto its = itn->second.begin(); its != itn->second.end(); its++)
            (*its)->setFadeout(fade_s);
}
