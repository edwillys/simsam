#pragma once

#include "AudioAtom.h"
#include "Sample.h"
#include <vector>
#include <map>
#include <string>
#include <memory>

class CSimSamProps : public CAudioAtomProps
{
public:
    int32_t m_Polyphony;
    std::string m_FilePath;

    CSimSamProps() : m_Polyphony(64) {}
    ~CSimSamProps(){}
};

class CSimSam : public CAudioAtom
{
private:
    std::map<int32_t, std::vector<std::shared_ptr<CSample> > > m_Notes;
    std::vector<std::shared_ptr<CSample> > m_PlayingNotes;
    int32_t m_Polyphony;
    bool_t m_LoadPending;
    std::string m_Path;

    /**
     * @brief 
     * 
     * @param sample 
     */
    void noteOn(std::shared_ptr<CSample> sample);

    /**
     * @brief 
     * 
     * @param pitch 
     */
    void noteOff(cint32_t pitch);

    /**
     * @brief Check if a given pitch is a note
     * 
     * @param pitch 
     * @return 0 if not found 
     * @return reference pitch if not found (can be different from argument)
     */
    cint32_t getRefPitch(cint32_t pitch);

    /**
     * @brief 
     * 
     * @param pitch 
     * @param velocity 
     */
    void midiOn(const int32_t pitch, const int32_t velocity);

    /**
     * @brief 
     * 
     * @param pitch 
     */
    void midiOff(const int32_t pitch);

public:
    /**
     * @brief 
     * 
     */
    typedef enum
    {
        SET_INTVEC_MIDI = 0,
    }ESetIntVectorIndex;

    /**
     * @brief 
     * 
     */
    typedef enum
    {
        SET_FLOATVEC_ADSP = 0,
        SET_FLOATVEC_FADEOUT
    }ESetFloatVecIndex;

    /**
     * @brief 
     * 
     */
    typedef enum
    {
        SET_MIDI_NOTE_ON = 0,
        SET_MIDI_NOTE_OFF,
    }ESfzpNoteAction;

    /**
     * @brief Construct a new CSimSam object
     * 
     */
    CSimSam(){};

    /**
     * @brief Destroy the CSimSam object
     * 
     */
    ~CSimSam(){};

    /**
     * @brief 
     * 
     * @param props 
     */
    void init(const CSimSamProps& props);
    
    /**
     * @brief 
     * 
     * @param in 
     * @param out 
     */
    void play(float32_t ** const in, float32_t ** const out);

    /**
     * @brief 
     * 
     * @param index 
     * @param values 
     */
    void set(const int32_t index, const std::vector<int32_t>& values);

    /**
     * @brief 
     * 
     * @param index 
     * @param values 
     */
    void set(const int32_t index, const std::vector<float32_t>& values);
    
    /**
     * @brief Get the amount of notes currently plaing
     * 
     * @return int32_t 
     */
    int32_t getNumNotesPlaying( void ) {return m_PlayingNotes.size();};

    /**
     * @brief Get the Polyphony order
     * 
     * @return int32_t 
     */
    int32_t getPolyphony( void ) {return m_Polyphony;};

    /**
     * @brief Ser the Polyphony order
     * 
     * @param polyphony 
     */
    void setPolyphony( int32_t polyphony ) { m_Polyphony = polyphony; };

    /**
     * @brief Set the File Path for the SFZ file
     * 
     * @param path 
     */
    void setFilePath(const std::string &path);

    /**
     * @brief Set the global ADSR parameters
     * 
     * @param adsr 
     */
    void setADSR(const CSample::CADSR& adsr);

    /**
     * @brief Set the global fadeout parameter
     * 
     * @param adsr 
     */
    void setFadeout(cfloat32_t fade_s);

    /**
     * @brief Whether SFZ player has loaded the samples 
     * 
     * @return bool_t 
     */
    bool_t isLoadPending(void) { return m_LoadPending;};

    /**
     * @brief Load the samples from the previously set SFZ file
     * 
     * @return int32_t 
     */
    int32_t loadSamples(void);
};
