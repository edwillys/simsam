function (event) {
    
    function handle_event (event, value) {
        ptr_events_val = "auto";
        if (value == 0){
            // print READY
            event.icon.find ('[mod-role=status]').text ("READY");
        }
        else if (value == 1){
            // print LOADING SAMPLES...
            event.icon.find ('[mod-role=status]').text ("LOADING SAMPLES...");
            // momentarily disable click on all input control ports
            ptr_events_val = "none";
        }
        else if (value == 2){
            event.icon.find ('[mod-role=status]').text ("LOADING FAILED!");
        }

        // re-set click CSS property on all input control ports
        ctl_ports = event.icon.find('[mod-role=input-control-port]');
        for (let [key, val] of Object.entries(ctl_ports)) {
            if (typeof val === 'object' && val !== null && val.tagName == 'DIV'){
                val.style.pointerEvents = ptr_events_val;
            }
        }
    }

    if (event.type == 'start' )
    {
        for (var i = 0; i < event.ports.length; i++){
            if(event.ports[i].symbol == 'sfz_status'){
                handle_event(event, event.ports[i].value);
                break;
            }
        };
    } 
    else if (event.type == 'change' && event.symbol == 'sfz_status') {
        handle_event(event, event.value);
    }
}
