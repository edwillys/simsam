#ifndef SFZ_TESTS
#define SFZ_TESTS
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <math.h>
#include <algorithm>
#include "SfzParser.h"
#include "SimSam.h"
#include <iostream>
#include <vector>
#include "wave/file.h"
#include <linux/limits.h>
#include "TestUtils.h"

//=============================================================
// Reference audio files
//=============================================================
namespace NA4v1{
#include "ref/SalamanderGrandPianoV3/A4v1.h"
}
namespace NC4v1{
#include "ref/SalamanderGrandPianoV3/C4v1.h"
}
namespace NA4v16{
#include "ref/SalamanderGrandPianoV3/A4v16.h"
}
namespace NC4v16{
#include "ref/SalamanderGrandPianoV3/C4v16.h"
}
namespace NDSharp4v1{
#include "ref/SalamanderGrandPianoV3/DSharp4v1.h"
}
namespace NFSharp4v1{
#include "ref/SalamanderGrandPianoV3/FSharp4v1.h"
}
namespace NC8v16{
#include "ref/SalamanderGrandPianoV3/C8v16.h"
}
namespace NADSR{
#include "ref/adsr.h"
}
namespace NADSR1ms{
#include "ref/adsr1ms.h"
}
namespace NADSRAtt{
#include "ref/adsr_attack_only.h"
}
namespace NADSRAttDec{
#include "ref/adsr_attack_decay.h"
}
namespace NADSRAttRel{
#include "ref/adsr_attack_release.h"
}
namespace NADSRDec{
#include "ref/adsr_decay_only.h"
}
namespace NADSRDecRel{
#include "ref/adsr_decay_release.h"
}
namespace NADSRRel{
#include "ref/adsr_release_only.h"
}
namespace NADSRAtt2Large{
#include "ref/adsr_attack_toolarge.h"
}
namespace NADSRRel2Large{
#include "ref/adsr_release_toolarge.h"
}
namespace NADSRAttRel2Large{
#include "ref/adsr_attack_release_toolarge.h"
}
namespace NE5v16fromA4v16{
#include "ref/SalamanderGrandPianoV3/A4v16_up5th.h"
}

//=============================================================
// Defines
//=============================================================
typedef struct
{
    int32_t nsamples;
    int32_t *samples;
} tTestNotes;

template <class T>
struct tTestPayloadSequence
{
    int32_t timestamp_block;
    std::vector<T> payload;
};

BOOST_AUTO_TEST_SUITE (SfzTests)

//=============================================================
// Helper functions
//=============================================================

/**
 * @brief 
 * 
 * @param notes 
 * @return std::vector<int32_t> 
 */
static std::vector<int32_t> sumnotes(std::vector<tTestNotes>& notes)
{
    int32_t max_nsamples = INT32_MIN;

    for( auto it = notes.begin(); it != notes.end(); it++)
        max_nsamples = std::max(max_nsamples, it->nsamples);

    std::vector<int32_t>ret(max_nsamples, 0);

    for(auto it = notes.begin(); it != notes.end(); it++)
    {
        for(auto j = 0; j < it->nsamples; j++)
            ret[j] += it->samples[j];
    }

    return ret;
}

/**
 * @brief 
 * 
 * @param sfzp 
 * @param payload_seq 
 * @param ind_block 
 */
static void check_set_midi_payload(CSimSam& sfzp, 
    const std::vector<tTestPayloadSequence<int32_t> >& payload_seq, cint32_t ind_block)
{
    for(auto it = payload_seq.begin(); it != payload_seq.end(); it++)
    {
        if (it->timestamp_block == ind_block)
        {
            sfzp.set(CSimSam::ESetIntVectorIndex::SET_INTVEC_MIDI, it->payload);
            int32_t num_notes =  std::min((int32_t)it->payload.size() / 3, sfzp.getPolyphony());
            BOOST_CHECK_GE (sfzp.getNumNotesPlaying(), num_notes);
        }
    }
}

static void check_set_float_payload(CSimSam& sfzp, 
    const std::vector<tTestPayloadSequence<float32_t> >& payload_seq, cint32_t ind_block, cint32_t cmd)
{
    for(auto it = payload_seq.begin(); it != payload_seq.end(); it++)
    {
        if (it->timestamp_block == ind_block)
        {
            sfzp.set(cmd, it->payload);
        }
    }
}

void compare_samples( float32_t *a, int32_t *b, cint32_t bs, cint32_t eps, bool_t writeout, 
    float32_t * content)
{
    for(auto j = 0; j < bs; j++)
    {
        int16_t sample;
        if ( a[j] >= 0.0F)
            sample = (int16_t)(a[j] * INT16_MAX);
        else
            sample = (int16_t)(a[j] * (-INT16_MIN));

        if (writeout){
            content[j] = a[j];
            BOOST_CHECK_LE ( abs(b[j] - sample), eps);
        } else {
            BOOST_REQUIRE_LE ( abs(b[j] - sample), eps);
        }
    }
}

void compare_samples( float32_t *a, float32_t *b, cint32_t bs, cfloat32_t eps, bool_t writeout, 
    float32_t * content)
{
    for(auto j = 0; j < bs; j++)
    {
        if (writeout) {
            BOOST_CHECK_LE ( abs(b[j] - a[j]), eps);
            content[j] = a[j];
        } else {
            BOOST_REQUIRE_LE ( abs(b[j] - a[j]), eps);
        }
    }
}

/**
 * @brief 
 * 
 * @param sfzp 
 * @param outputs 
 * @param bs 
 * @param samples_perch 
 * @param samples 
 * @param payload_seq 
 * @param outwav 
 * @param eps 
 */
template<class T>
static void helper_test_playnotes(CSimSam& sfzp, float32_t **outputs, cint32_t bs,
    cint32_t samples_perch, T *samples, 
    const std::vector<tTestPayloadSequence<int32_t> >& payload_midi, 
    const std::vector<tTestPayloadSequence<float32_t> >& payload_adsr,
    const std::vector<tTestPayloadSequence<float32_t> >& payload_fadeout,
    const T eps, std::string outwav = "")
{
    std::vector<float> content; // in case we are writing output WAV
    bool_t writeout = outwav.size() > 0;

    BOOST_CHECK_EQUAL (sfzp.getNumNotesPlaying(), 0);

    int32_t numblocks = ceil((float32_t)samples_perch / (float32_t)bs);
    if (writeout)
        content.resize(numblocks * bs);
    // play and check all but last block
    for(auto i = 0; i < numblocks - 1; i++)
    {
        check_set_float_payload(sfzp, payload_adsr, i, CSimSam::ESetFloatVecIndex::SET_FLOATVEC_ADSP);
        check_set_float_payload(sfzp, payload_fadeout, i, CSimSam::ESetFloatVecIndex::SET_FLOATVEC_FADEOUT);
        check_set_midi_payload(sfzp, payload_midi, i);

        sfzp.play(NULL, outputs);
        compare_samples(outputs[0], &samples[bs*i], bs, eps, writeout, &content[bs*i]);
    }
    // play and check remaining samples from last block
    int32_t remaining = samples_perch - ((numblocks - 1) * bs);
    check_set_float_payload(sfzp, payload_adsr, (numblocks - 1), CSimSam::ESetFloatVecIndex::SET_FLOATVEC_ADSP);
    check_set_float_payload(sfzp, payload_fadeout, (numblocks - 1), CSimSam::ESetFloatVecIndex::SET_FLOATVEC_FADEOUT);
    check_set_midi_payload(sfzp, payload_midi, (numblocks - 1));
    sfzp.play(NULL, outputs);
    for(auto j = 0; j < remaining; j++)
    {
        int16_t sample;
        if ( outputs[0][j] >= 0.0F)
            sample = (int16_t)(outputs[0][j] * INT16_MAX);
        else
            sample = (int16_t)(outputs[0][j] * (-INT16_MIN));
        BOOST_CHECK_LE ( abs(samples[bs * (numblocks - 1) + j] - sample), eps);
        if (writeout)
            content[bs * (numblocks - 1) + j] = outputs[0][j];
    }
    
    // write output to wav
    if (writeout)
        write_wav(outwav, content);

    BOOST_CHECK_EQUAL (sfzp.getNumNotesPlaying(), 0);
}

//=============================================================
// Test cases
//=============================================================

/**
 * @brief Test case: playing notes without ADSR
 * 
 */
BOOST_AUTO_TEST_CASE (SimSamPlayNotesNoADSR)
{
    CSimSam sfzp = CSimSam();
    CSimSamProps props;
    cint32_t bs = 64;
    cint32_t nout = 1;
    std::vector<int32_t> ref_samples;
    std::vector<tTestPayloadSequence<int32_t> > payload_midi;
    std::vector<tTestPayloadSequence<float32_t> > payload_adsr;
    std::vector<tTestPayloadSequence<float32_t> > payload_fadeout;
    std::vector<tTestNotes> notes;
    cint32_t EPS = 1;
    
    props.m_BlockSize = bs;
    props.m_Fs = 48000;
    props.m_NumChOut = nout;
    props.m_NumCtrlIn = 1;
    props.m_Polyphony = 4;
    props.m_FilePath = "test_samples.sfz";

    float32_t **outputs = new float32_t *[nout];
    for( auto i = 0; i < nout; i++)
        outputs[i] = new float32_t[bs];

    sfzp.init(props);
    sfzp.loadSamples();
    
    // 100ms for every test
    payload_fadeout = {
        {0,
            { 0.0F } // TODO
        }
    };

    // only A0v1
    payload_midi = {
        {0, 
            {
                69, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NA4v1::samples_per_ch, NA4v1::samples[0], 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // only C8v16
    payload_midi = {
        {0, 
            {
                108, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 121
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NC8v16::samples_per_ch, NC8v16::samples[0], 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // A0v1 + C8v16
    payload_midi = {
        {0,
            {
                69, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10, 
                108, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 121
            }
        }
    };
    notes = {
        {NA4v1::samples_per_ch , NA4v1::samples[0] },
        {NC8v16::samples_per_ch, NC8v16::samples[0]}
    };
    ref_samples = sumnotes(notes);
    helper_test_playnotes(sfzp, outputs, bs, 
        ref_samples.size(), ref_samples.data(), 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // C4v1 + D#4v1 + F#4v1 + A4v1
    payload_midi = {
        {0,
            {
                59, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10, 
                62, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10, 
                65, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10,
                68, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10
            }
        }
    };
    notes = {
        {NC4v1::samples_per_ch     , NC4v1::samples[0]      },
        {NDSharp4v1::samples_per_ch, NDSharp4v1::samples[0] },
        {NFSharp4v1::samples_per_ch, NFSharp4v1::samples[0] },
        {NA4v1::samples_per_ch     , NA4v1::samples[0]      }
    };
    ref_samples = sumnotes(notes);
    helper_test_playnotes(sfzp, outputs, bs, 
        ref_samples.size(), ref_samples.data(), 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // D#4v1 + (C4v1) + F#4v1 + A4v1 + C8v16
    payload_midi = {
        {0, 
            {
                62 , CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10, 
                59 , CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10, 
                65 , CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10,
                68 , CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10,
                108, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 121
            }
        }
    };
    notes = {
        //{NC4v1::samples_per_ch     , NC4v1::samples[0]      },
        {NDSharp4v1::samples_per_ch, NDSharp4v1::samples[0] },
        {NA4v1::samples_per_ch     , NA4v1::samples[0]      },
        {NFSharp4v1::samples_per_ch, NFSharp4v1::samples[0] },
        {NC8v16::samples_per_ch    , NC8v16::samples[0]     }
    };
    ref_samples = sumnotes(notes);
    helper_test_playnotes(sfzp, outputs, bs, 
        ref_samples.size(), ref_samples.data(), 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // Test sequence
    payload_midi = {
        // start off on block index 0: A4v1 + C4v16 playing
        {0,
            {
                69, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10, 
                60, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 121
            }
        },
        // after 75 blocks (100ms), we override with same notes, but different velocities
        {75,
            {
                69, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 121, 
                60, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10
            }
        },
        // at timestamp 150 blocks (200ms), we turn off both notes
        {150,
            {
                60, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_OFF,
                69, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_OFF
            }
        },
    };
    notes = {
        {4800, NA4v1::samples[0] },
        {4800, NC4v16::samples[0]}
    };
    ref_samples = sumnotes(notes);
    notes = {
        {4800, NA4v16::samples[0]},
        {4800, NC4v1::samples[0]}
    };
    // insert second sequence
    auto more_samples = sumnotes(notes);
    ref_samples.insert(ref_samples.end(), more_samples.begin(), more_samples.end());
    // fill the rest with zeros
    ref_samples.resize(3 * 4800, 0);
    helper_test_playnotes(sfzp, outputs, bs, 
        ref_samples.size(), ref_samples.data(), 
        payload_midi, payload_adsr, payload_fadeout, 2);

    // only E5v1, from A4v1 up a 5th
    payload_midi = {
        {0, 
            {
                76, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 121
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NE5v16fromA4v16::samples_per_ch, NE5v16fromA4v16::samples[0], 
        payload_midi, payload_adsr, payload_fadeout, (float32_t)4.E-5 );

    for( auto i = 0; i < nout; i++)
        delete[] outputs[i];
    delete[] outputs;
}

/**
 * @brief Test case: ADSR
 * 
 */
BOOST_AUTO_TEST_CASE (SimSamADSR)
{
    CSimSam sfzp = CSimSam();
    CSimSamProps props;
    cint32_t bs = 64;
    cint32_t nout = 1;
    std::vector<tTestPayloadSequence<int32_t> > payload_midi;
    std::vector<tTestPayloadSequence<float32_t> > payload_adsr;
    std::vector<tTestPayloadSequence<float32_t> > payload_fadeout;
    cint32_t EPS = 6;
        
    props.m_BlockSize = bs;
    props.m_Fs = 48000;
    props.m_NumChOut = nout;
    props.m_NumCtrlIn = 1;
    props.m_FilePath = "test_adsr.sfz";

    float32_t **outputs = new float32_t *[nout];
    for( auto i = 0; i < nout; i++)
        outputs[i] = new float32_t[bs];

    sfzp.init(props);
    sfzp.loadSamples();
    
    // main note that is played in almost all sub test cases
    payload_midi = {
        {0, 
            {
                21, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10
            }
        }
    };
    
    // complete ADSR
    payload_adsr = {
        {0, 
            {
                0.1F, 0.1F, 0.8F, 0.2F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSR::samples_per_ch, NADSR::samples[0], 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // complete ADSR ( 1ms precision)
    payload_adsr = {
        {0, 
            {
                0.151F, 0.049F, 0.8F, 0.149F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSR1ms::samples_per_ch, NADSR1ms::samples[0], 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // attack only
    payload_adsr = {
        {0, 
            {
                0.1F, 0.0F, 1.0F, 0.0F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRAtt::samples_per_ch, NADSRAtt::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);
    
    // decay only
    payload_adsr = {
        {0, 
            {
                0.0F, 0.1F, 0.8F, 0.0F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRDec::samples_per_ch, NADSRDec::samples[0],
       payload_midi, payload_adsr, payload_fadeout, EPS);

    // release only
    payload_adsr = {
        {0, 
            {
                0.0F, 0.0F, 1.0F, 0.2F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRRel::samples_per_ch, NADSRRel::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // attack and decay
    payload_adsr = {
        {0, 
            {
                0.1F, 0.1F, 0.8F, 0.0F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRAttDec::samples_per_ch, NADSRAttDec::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // decay and release
    payload_adsr = {
        {0, 
            {
                0.0F, 0.1F, 0.8F, 0.2F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRDecRel::samples_per_ch, NADSRDecRel::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // attack and release
    payload_adsr = {
        {0, 
            {
               0.1F, 0.0F, 1.0F, 0.2F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRAttRel::samples_per_ch, NADSRAttRel::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // attack too large
    payload_adsr = {
        {0, 
            {
               1.2F, 0.0F, 1.0F,  0.0F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRAtt2Large::samples_per_ch, NADSRAtt2Large::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);
    
    // release too large
    payload_adsr = {
        {0, 
            {
                0.0F, 0.0F, 1.0F, 1.2F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRRel2Large::samples_per_ch, NADSRRel2Large::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);
    
    // attack+release too large
    payload_adsr = {
        {0, 
            {
                0.6F, 0.0F, 1.0F,  0.6F
            }
        }
    };
    helper_test_playnotes(sfzp, outputs, bs, 
        NADSRAttRel2Large::samples_per_ch, NADSRAttRel2Large::samples[0],
        payload_midi, payload_adsr, payload_fadeout, EPS);

    // complete ADSR + changing to release only in the middle
    payload_midi = {
        {0, 
            {
                21, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10
            }
        },
        // repeat the same note after 1s (end of first one)
        {750, 
            {
                21, CSimSam::ESfzpNoteAction::SET_MIDI_NOTE_ON, 10
            }
        }
    };
    payload_adsr = {
        {0, 
            {
                0.1F, 0.1F, 0.8F,  0.2F
            }
        },
        {375, 
            {
                0.0F, 0.0F, 1.0F,  0.2F
            }
        }
    };
    std::vector<int32_t> samples = std::vector<int32_t>(NADSR::samples[0], NADSR::samples[0] + NADSR::samples_per_ch);
    std::vector<int32_t> more_samples =  std::vector<int32_t>(NADSRRel::samples[0], NADSRRel::samples[0] + NADSRRel::samples_per_ch);
    samples.insert(samples.end(), more_samples.begin(), more_samples.end());
    helper_test_playnotes(sfzp, outputs, bs, 
        samples.size(), samples.data(), 
        payload_midi, payload_adsr, payload_fadeout, EPS);

    for( auto i = 0; i < nout; i++)
        delete[] outputs[i];
    delete[] outputs;
}

BOOST_AUTO_TEST_SUITE_END()

#endif //SFZ_TESTS
