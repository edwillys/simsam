# SimSam

**A simple sampler LV2 plug-in based on SFZ files**

![](bundles/simsam.lv2/modgui/screenshot-sfz-player.png)

For supported samples, see the table below. The references to the original files are mentioned on the repos. 

The WAV files have been converted to mono PCM16 in order to reduce the memory footprint. The padding zeros at the end have been trimmed.

The missing notes are filled with the ones available, as long as they can be resampled from lower/upper 4th, lower/upper 5th. Otherwise, they are left empty.

ADSR enveloping is supported, though, for the piano samples, the fadeout parameters should be enough. If attack, release and decay are left to 0ms, the sustain level acts as a simple gain and the complete WAV note is played as long as it is 'on'.

The SFZ parser is very simple and currently supports a subset of SFZv1. This is enough for the included SFZ files. 

**This plug-in depends on libsndfile**

### Update submodules that you're planning to use
If you have not cloned this repo recursively (which would include all referenced samples), you can do it individually via

`git submodule update --init -- <rel_path_see_below>`

| Index | Name | Relative Path | Size | Repo |
| ------ | ------ | ------ | ------ | ------ |
| 0 | Salamander Grand Piano V3 | bundles/simsam.lv2/sfz/salamandergrandpiano | ~623MB | https://gitlab.com/edwillys/salamandergrandpiano |
| 1 | Francis Bacon Piano | bundles/simsam.lv2/sfz/francisbaconpiano | ~55MB | https://gitlab.com/edwillys/francisbaconpiano |
| 2 | Steinway&SonsModel B | bundles/simsam.lv2/sfz/SteinwaySonsModelB | ~723MB | https://gitlab.com/edwillys/SteinwaySonsModelB |

### Build the LV2 plugin:

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

### Build the tests
```
cd tests
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```


### TODO
- Implement velocity modulation
- Add further samples
